from django.urls import path
from . import views

app_name = 'personalwebsite'

urlpatterns = [
    path('', views.welcome, name='welcome'),
    path('profil/', views.profile, name='profile'),
    path('experience/', views.experience, name='experience')
]