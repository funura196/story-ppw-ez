from django.shortcuts import render

# Create your views here.

def welcome(request):
    return render(request, 'personalwebsite/welcome.html')

def profile(request):
    return render(request, 'personalwebsite/profile.html')

def experience(request):
    return render(request, 'personalwebsite/experience.html')

