from django.urls import path
from . import views

app_name = 'matkul'

urlpatterns = [
    path('matkul',views.listMatkul,name='listMatkul'),
    path('TambahkanMatkul',views.add, name='addMatkul'),
    path('delete/P<int:delete_id>/',views.delete, name='delete'),
]