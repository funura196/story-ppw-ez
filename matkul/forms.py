from django import forms
from .models import PostModel


class MatkulForm(forms.Form):
    nama_matkul = forms.CharField(
        label = "Nama Mata Kuliah",
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )
    nama_dosen = forms.CharField(
    label = "Nama Dosen",
    max_length = 100,
    widget = forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )
    jumlah_sks = forms.IntegerField(
        label = "Jumlah SKS",
        widget = forms.NumberInput(
            attrs={
                'class':'form-control',
                'min':'1',
                'max':'6',
                'type':'number'
            }
        )
    )
    semester = forms.CharField(
    label = "Semester Tahun",
    max_length = 100,
    widget = forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )
    ruang_kelas = forms.CharField(
    label = "Ruang Kelas",
    max_length = 100,
    widget = forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )
    deskripsi = forms.CharField(
    label = "Deskripsi",
    max_length = 100,
    widget = forms.Textarea(
            attrs={
                'class':'form-control',
            }
        )
    )